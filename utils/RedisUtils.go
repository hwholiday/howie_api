package utils

import (
	"github.com/astaxie/beego"
	"time"
	"github.com/hwholiday/howie/utils/cache"
)

var redis cache.BaseRedis

func init() {
	var redisInit cache.RedisInit
	var err error
	redisInit.Addr = beego.AppConfig.String("redisaddr")
	redisInit.Prefix = "howie_api"
	redis, err = cache.InitRedisClient(&redisInit)
	if err != nil {
		beego.Error(err.Error())
	}
}

func Set(key string, value interface{}, expiration time.Duration) (err error) {
	if err = redis.SetValue(key, value, expiration); err != nil {
		return
	}
	return
}

func Get(key string) (value string, err error) {
	if value, err = redis.GetByKey(value + key); err != nil {
		return
	}
	return
}
func Del(key string) (err error) {
	if err = redis.DelByKey(key); err != nil {
		return
	}
	return
}
