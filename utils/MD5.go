package utils

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/astaxie/beego"
)

//加密密码
func MD5ToString(data string) string {
	key := beego.AppConfig.String("pwdmd5key")
	if key==""{
		key="aba42x&(aBUaX#ax%80"
	}
	h := md5.New()
	h.Write([]byte(data + key))
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr) // 输出加密结果
}
