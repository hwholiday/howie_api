package main

import (
	_ "howie_api/routers"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
	"fmt"
	"github.com/astaxie/beego/orm"
)

//#bee generate appcode -tables="user" -driver=mysql -conn="root:root@tcp(127.0.0.1:3306)/howie" -level=2
func main() {

	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}

func init() {
	isDev := beego.BConfig.RunMode == "dev"
	// db
	link := fmt.Sprintf("%s:%s@(%s:%s)/%s", beego.AppConfig.String("mysqluser"),
		beego.AppConfig.String("mysqlpass"), beego.AppConfig.String("mysqlurls"),
		beego.AppConfig.String("mysqlport"), beego.AppConfig.String("mysqldb"))
	orm.RegisterDataBase("default", "mysql", link)
	orm.Debug = isDev
}
