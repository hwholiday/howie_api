package test3

import (
	"reflect"
	"fmt"
	"testing"
)

type User struct {
	Name   string "name" //这引号里面的就是tag
	Passwd string "passsword"
}
type Game struct {
}

func TestR(t *testing.T) {
	user := &User{"chronos", "pass"}
	s := reflect.TypeOf(user).Elem() //通过反射获取type定义
	for i := 0; i < s.NumField(); i++ {
		fmt.Println(s.Field(i).Tag) //将tag输出出来
	}
}

func TestGow(t *testing.T) {
	var f interface{}
	f = 1
	if v, ok := f.(int); ok {
       fmt.Println(v)
	}

}
