package controllers

import (
	"howie_api/models"
	"howie_api/service"
	"strconv"
	"fmt"
)

//用户信息
type AuthControllers struct {
	BaserControllers
}

var channel = make(chan bool, 150)
// @Description 登录
// @Param	body	body 	models.UserData  true		""
// @Success 200     {object}   models.User
// @Failure 403     body is empty
// @router /login [post]
func (c *AuthControllers) Login() {
	var data models.UserData
	c.InputBaseData(&data)
	if data.Account == "" || data.Password == "" {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return

	}
	user, err := service.UserLogin(&data)
	if err != nil {
		c.RespFail(CODE_ERR_LOGIN_USER, err.Error())
		return

	}










	c.RespData(user)
	return
}

// @Description 创建用户
// @Param	body	body 	models.UserData  true		""
// @Success 200     {object}   models.User
// @Failure 403     body is empty
// @router /register [post]
func (c *AuthControllers) Registered() {
	c.GetString()
	var data models.UserData
	c.InputBaseData(&data)
	if data.Account == "" || data.Password == "" || data.CreateId == 0 {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return

	}
	fmt.Println(data)
	user, err := service.RegisteredUsers(&data)
	if err != nil {
		c.RespFail(CODE_ERR_REGISTER_USER, err.Error())
		return

	}
	c.RespData(user)
	return

}

// @Description 修改密码
// @Param	body	body 	models.UserData  true		""
// @Success 200     string OK
// @Failure 403     body is empty
// @router /change_pass [post]

func (c *AuthControllers) ChangePassWord() {
	var data models.UserData
	c.InputBaseData(&data)
	data.Uid = c.GetUid()
	if data.NewPassword == "" || data.Password == "" {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return

	}
	err := service.ChangePass(&data)
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return

	}
	c.RespSuccess()
	return

}

// GetAll ...
// @Title Get All
// @Description get User
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.User
// @Failure 403
// @router / [get]
func (c *AuthControllers) GetAll() {
	channel <- true
	var user []models.User
	err, total := c.GetAllAndTotal(new(models.User), &user)
	if err != nil {
		fmt.Println(err)
		c.RespFail(CODE_ERR, err.Error())
		<-channel
		return
	}
	c.RespDataWithPage(user, total)
	<-channel
	return

}

// DeleteUser ...
// @Title DeleteUser
// @Description get User
// @Param	id	path	int	true	"要删除用户的ID"
// @Success 200 string OK
// @Failure 403
// @router /:id [delete]
func (c *AuthControllers) DeleteUser() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	if id == 0 {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return
	}
	err := models.DeleteUser(id)
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return

	}
	c.RespSuccess()
	return

}

// GetAdministerList ...
// @Title GetAdministerList
// @Description get User
// @Success 200 string OK
// @Failure 403
// @router /administer_list [get]
func (c *AuthControllers) GetAdministerList() {
	err, data := service.GetAdministerList()
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return

	}
	c.RespData(data)
	return

}

// ChangeLoginType ...
// @Title ChangeLoginType
// @Description get User
// @Param	id	path	int	true	"用户的ID"
// @Success 200 string OK
// @Failure 403
// @router /login_type/:id [put]
func (c *AuthControllers) ChangeLoginType() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	if id == 0 {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return
	}
	err := models.ChangeLoginType(id)
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return

	}
	c.RespSuccess()
	return

}

// DeleteMachineCode ...
// @Title DeleteMachineCode
// @Description get User
// @Param	id	path	int	true	"要清除用户的ID"
// @Success 200 string OK
// @Failure 403
// @router /:id [put]
func (c *AuthControllers) DeleteMachineCode() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	if id == 0 {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return
	}
	err := models.DeleteMachineCode(id)
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return

	}
	c.RespSuccess()
	return

}

// AddTime...
// @Title AddTime
// @Description get User
// @Param	id	path	int	true
// @Param	add_time	query	int	true	"要添加的时间"
// @Success 200 string OK
// @Failure 403
// @router /add_time/:id [put]
func (c *AuthControllers) AddTime() {
	idStr := c.Ctx.Input.Param(":id")
	addTime := c.GetString("add_time", "0")
	id, _ := strconv.Atoi(idStr)
	if id == 0 {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return
	}
	add, err := strconv.ParseInt(addTime, 10, 64)
	if add == 0 {
		c.RespFail(CODE_INCORRECT_PARAMETER, "参数不完整")
		return

	}
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return
	}
	err = models.AddTime(id, add)
	if err != nil {
		c.RespFail(CODE_ERR, err.Error())
		return

	}
	c.RespSuccess()
	return

}
