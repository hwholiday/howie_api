package controllers

const (
	CODE_TOKEN_ERR           = 333 //操作失败
	CODE_ERR                 = 402 //操作失败
	CODE_INCORRECT_PARAMETER = 401 //参数不正确
	CODE_ERR_LOGIN_USER      = 403 //用户名或者密码错误
	CODE_ERR_REGISTER_USER   = 402 //用户注册失败
)
