package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "Registered",
			Router: `/register`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "DeleteUser",
			Router: `/:id`,
			AllowHTTPMethods: []string{"delete"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "GetAdministerList",
			Router: `/administer_list`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "ChangeLoginType",
			Router: `/login_type/:id`,
			AllowHTTPMethods: []string{"put"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "DeleteMachineCode",
			Router: `/:id`,
			AllowHTTPMethods: []string{"put"},
			Params: nil})

	beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"] = append(beego.GlobalControllerRouter["howie_api/controllers:AuthControllers"],
		beego.ControllerComments{
			Method: "AddTime",
			Router: `/add_time/:id`,
			AllowHTTPMethods: []string{"put"},
			Params: nil})

}
